# README #

Automation Framework for Amaysim Automation assessment

Currently, the tests are failing due to test failures (not app failures).
But I believe there's enough in here for you to be able to assess my skills as an Automation Tester.

Framework was created using Selenium WebDriver and coded in Java.

To run the tests:
1. In eclipse, get TestNG plugin
2. Add external JARs from myamaysim/lib
3. Run test in myamaysim/src/test/* as TestNG test
- Right click
- Run As
- TestNG Test

Not sure if it can be run in cmd, but try by:
1. Add TestNG to classpath
2. In cmd, go to folder
3. Type in org.testng.TestNG <test>

framework directory:
- myamaysim/src/com/amaysim/

test directory:
- myamaysim/src/test

Send me an email: e.c.bello@live.com