# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

My Amaysim automation assessment
Version 1.0

### How do I get set up? ###

Add test-ng to class path (in lib)
run using: org.testng.TestNg test/AccountSettingsTest.java

if that doesn't work, open as a project in Eclipse
make sure that test-ng plugin is installed
make sure jar dependencies in /lib are all included
open test file, right-click, Run As, TestNG