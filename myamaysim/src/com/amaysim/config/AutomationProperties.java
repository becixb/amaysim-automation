package com.amaysim.config;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Functions for the properties file of Automation
 * 
 * @author ecbello
 *
 */
public class AutomationProperties {
	private static final String PROP_FILE = "automation.properties";
	
	/*
	 * Get all the properties specified in the properties file
	 */
    public static Properties getProperties() throws Exception {
    	Properties prop = new Properties();
        InputStream inputStream = null;
        
        try {
        	inputStream = AutomationProperties.class.getResourceAsStream(PROP_FILE);
        	if (inputStream != null) {
        		prop.load(inputStream);
        	} else {
        		 throw new FileNotFoundException("Cannot find property file '" + PROP_FILE + "'.");
        	}
        	return prop;
        } catch (Exception e) {
        	throw(e);
        } finally {
        	if (inputStream != null) {
        		inputStream.close();
        	}
        }
    }
    
    /*
     * Get a specific property specified in the properties file
     */
    public static String getProperty(String propertyKey) throws Exception {
    	Properties prop = getProperties();
    	return prop.getProperty(propertyKey);
    }
}
