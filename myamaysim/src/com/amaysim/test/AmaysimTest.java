package com.amaysim.test;

import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public class AmaysimTest {
	private Assertion asrt = new Assertion();
	private SoftAssert vrfy = new SoftAssert();
	
	public void verifyEqual(String msg, String expected, String actual) {
		vrfy.assertEquals(expected, actual, msg);
	}
	
	public void verifyEqual(String msg, Object expected, Object actual) {
		vrfy.assertEquals(expected, actual, msg);
	}
	
	public void verifyTrue(String msg, boolean cond) {
		vrfy.assertTrue(cond, msg);
	}
	
	public void verifyFalse(String msg, boolean cond) {
		vrfy.assertTrue(!cond, msg);
	}
	
	public void assertEqual(String msg, String expected, String actual) {
		asrt.assertEquals(expected, actual, msg);
	}
	
	public void assertTrue(String msg, boolean cond) {
		asrt.assertTrue(cond, msg);
	}
	
	public void assertFalse(String msg, boolean cond) {
		asrt.assertTrue(!cond, msg);
	}	
}
