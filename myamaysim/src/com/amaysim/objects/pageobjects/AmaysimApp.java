package com.amaysim.objects.pageobjects;

public class AmaysimApp {
	public BasePage browser;
	public boolean isLoggedIn;
	
	public AmaysimApp(BasePage browser) {
		this.browser = browser;
		browser.setBaseUrl("https://www.amaysim.com.au");
	}
	
	public AmaysimApp navigate() {
		browser.navigate(browser.baseUrl);
		return this;
	}
}
