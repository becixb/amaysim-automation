package com.amaysim.objects.pageobjects.enums;

import java.util.HashMap;

public enum PaymentType {
	CREDIT_DEBIT_CARD("Credit/Debit card", 1, "Card"),
	DIRECT_DEBIT("Direct debit from bank account", 2, "Debit from account"),
	BPAY_VOUCHERS("BPAY / Vouchers", 3, "Bpay"),
	PAYPAL("PayPal", 4, "PayPal");
	
	private static final HashMap<String, PaymentType> map = new HashMap<>(values().length, 1);
	private String name;
	private Integer value;
	private String shortName;
	
	static {
		for (PaymentType payment : values()) map.put(payment.shortName, payment);
	}
	
	private PaymentType(String name, Integer value, String shortName) {
		this.name = name;
		this.value = value;
		this.shortName = shortName;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getShortName() {
		return shortName;
	}
	
	public static PaymentType of(String shortName) {
		return map.get(shortName);
	}
}
