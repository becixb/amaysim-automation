package com.amaysim.objects.pageobjects.myaccount;

import org.openqa.selenium.By;

import com.amaysim.objects.pageobjects.BasePage;

public class AccOverviewPage extends MyAccountPage {
	private String welcomePopup = "//div[@id='welcome_popup']";
	
	public AccOverviewPage(BasePage browser) {
		super(browser);
	}
	
	public AccOverviewPage closeWelcomePopup() {
		browser.clickElement(By.xpath(welcomePopup + "//a[@class='close-reveal-modal']"));
		return this;
	}
}
