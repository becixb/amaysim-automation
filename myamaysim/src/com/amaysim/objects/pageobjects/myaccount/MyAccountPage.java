package com.amaysim.objects.pageobjects.myaccount;

import org.openqa.selenium.By;

import com.amaysim.objects.pageobjects.AmaysimApp;
import com.amaysim.objects.pageobjects.BasePage;

public class MyAccountPage extends AmaysimApp {
	private String menuItem = "//ul[@id='menu_list']/li/a";
	private String activePopup = "//div[contains(@class,'form_info_popup') and contains(@style,'display: block')]";
	
	public MyAccountPage(BasePage browser) {
		super(browser);
	}
	
	public AccSettingsPage openAccountSettings() {
		openMenuItem("My Settings");
		return new AccSettingsPage(browser);
	}
	
	public MyAccountPage closePopup() {
		browser.clickElement(By.xpath(activePopup + "/a"));
		return this;
	}
	
	public String getPopupMessage() {
		return browser.getText(By.xpath(activePopup + "/p"));
	}
	
	private void openMenuItem(String text) {
		try {
			browser.waitUntilElementIsVisible(By.xpath(menuItem + "[contains(., '" + text + "')]"), 3);	
		} catch (Exception e) {
			browser.clickElement("//div[text()='MENU']");
			browser.waitUntilElementIsVisible(By.xpath(menuItem), 2);
		}
//		if (!browser.doesElementExist(By.xpath(menuItem + "[contains(text(),'My Settings')]"))) {
//			browser.clickElement("//div[text()='MENU']");
//			browser.waitUntilElementIsVisible(By.xpath(menuItem), 2);
//		}
		browser.clickElement(menuItem + "[contains(., '" + text + "')]");
		finishLoading();
	}
	
	public MyAccountPage finishLoading() {
		browser.waitUntilElementIsVisible(By.xpath("//div[@id='ajax_loading' and contains(@style,'display:none')]"), 10);
		return this;
	}

}
