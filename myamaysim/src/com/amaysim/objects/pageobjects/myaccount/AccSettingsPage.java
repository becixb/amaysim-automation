package com.amaysim.objects.pageobjects.myaccount;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;

import com.amaysim.objects.pageobjects.BasePage;
import com.amaysim.objects.pageobjects.enums.PaymentType;

public class AccSettingsPage extends MyAccountPage {
	private static final String SIM_NICKNAME = "SIM Nickname";
	private static final String RECHARGE_PIN = "Recharge PIN";
	private static final String PUK_CODE = "PUK code";
	private static final String CALLER_ID = "Caller ID";
	private static final String CALL_WAITING = "Call waiting";
	private static final String VOICEMAIL = "Voicemail";
	private static final String USAGE_ALERTS = "Usage alerts";
	private static final String INTERNATIONAL_ROAMING = "International roaming";
	private static final String CALL_FORWARDING = "Call forwarding";
	private static final String PREMIUM_SMS_LIMIT = "Premium SMS limit";
	private static final String AUTO_RECHARGE = "Auto-recharge";
	private static final String CONFIRM = "//a[text()='Confirm']";
	
	public AccSettingsPage(BasePage browser) {
		super(browser);
	}
	
	public String getSimNickname() {
		return getValue(SIM_NICKNAME);
	}
	
	public PaymentType getPaymentType() {
		String shortName = browser.getText("//a[contains(@href,'payment_methods')]").split(" ")[1];
		return PaymentType.of(shortName);
	}
	
	public String getRechargePin() {
		return getValue(RECHARGE_PIN);
	}
	
	public String getPukCode() {
		return getValue(PUK_CODE);
	}
	
	public boolean isCallerIdEnabled() {
		return isChecked(CALLER_ID);
	}
	
	public boolean isCallWaitingEnabled() {
		return isChecked(CALL_WAITING);
	}
	
	public boolean isVoicemailEnabled() {
		return isChecked(VOICEMAIL);
	}
	
	public boolean isUsageAlertsEnabled() {
		return isChecked(USAGE_ALERTS);
	}
	
	public boolean isIntRoamingEnabled() {
		return isChecked(INTERNATIONAL_ROAMING);
	}
	
	public boolean isCallForwardingEnabled() {
		return getBoldValues(CALL_FORWARDING).equals("Yes");
	}
	
	public String getCallForwardingNumber() {
		return isCallForwardingEnabled() ? browser.getText("//div[div[text()='Call forwarding']]/following-sibling::div[1]/div").replace("Forward calls to", "").trim() : null;
	}
	
	public String getPremiumSmsLimit() {
		return getBoldValues(PREMIUM_SMS_LIMIT);
	}
	
	public boolean isAutoRechargeEnabled() {
		return getBoldValues(AUTO_RECHARGE).equals("Yes");
	}
	
	public Map<String,String> getAutoRechargeSettings() {
		if (isAutoRechargeEnabled()) {
			HashMap<String,String> autoRecharge = new HashMap<String,String>();
			String[] rechargeText = browser.getText("//div[div[text()='Auto-recharge']]/following-sibling::div[1]/div").replace("Recharge my mobile service with", "").replace("whenever the balance drops below","-").replace("/n","").replace(" ", "").split("-");
			autoRecharge.put("limit", rechargeText[1]);
			autoRecharge.put("recharge", rechargeText[0]);
			return autoRecharge;
		} else {
			return null;
		}
	}
	
	public AccSettingsPage setSimNickname(String nickname) {
		return clickEdit(SIM_NICKNAME).setField(SIM_NICKNAME, nickname).save(SIM_NICKNAME);
	}
	
	public AccSettingsPage setPlanSettings(PaymentType payType) {
		browser.clickElement("//a[contains(@href,'payment_methods')]");
		browser.clickElement("//a[text()='Change']");
		browser.selectOptionForSelect("payment_method_option", payType.getValue().toString());
		browser.clickElement(By.id("payment_app_form_button"));
		// Add payment method code here, for now let's just go back to settings page
		return openAccountSettings();
	}
	
	public AccSettingsPage setRechargePin(Integer pin) {
		return setRechargePin(pin.toString());
	}
	
	public AccSettingsPage setRechargePin(String pin) {
		return clickEdit(RECHARGE_PIN).setField(RECHARGE_PIN, pin).save(RECHARGE_PIN);
	}
	
	public AccSettingsPage setCallerId(boolean enable) {
		return setCheckbox(CALLER_ID, enable);
	}
	
	public AccSettingsPage setCallWaiting(boolean enable) {
		return setCheckbox(CALL_WAITING, enable);
	}

	public AccSettingsPage setVoicmail(boolean enable) {
		return setCheckbox(VOICEMAIL, enable);
	}
	
	public AccSettingsPage setUsageAlerts(boolean enable) {
		return setCheckbox(USAGE_ALERTS, enable);
	}
	
	public AccSettingsPage setInternationalRoaming(boolean enable) {
		return setCheckbox(INTERNATIONAL_ROAMING, enable);
	}
	
	public AccSettingsPage setCallForwarding(boolean enable, String number) {
		clickEdit(CALL_FORWARDING);
		browser.clickElement(By.xpath(CONFIRM));
		if (enable) {
			browser.inputText(By.xpath("//label[text()='Forward calls to']/following-sibling::input"), number);
		} else {
			browser.clickElement(By.xpath("//label[text()='Allow call forwarding']/following-sibling::div//input[@value='false']"));
		}
		return saveForm(CALL_FORWARDING);
	}
	
	public AccSettingsPage setPremiumSmsLimit(Integer limit) {
		clickEdit(PREMIUM_SMS_LIMIT);
		browser.selectOptionForSelect("my_amaysim2_setting_psms_spend",limit.toString());
		return saveForm(PREMIUM_SMS_LIMIT);	
	}
	
	public AccSettingsPage setAutoRecharge(boolean enable, Integer rechargeVal, Integer limit) {
		clickEdit(AUTO_RECHARGE);
		if (enable) {
			browser.selectOptionForSelect("my_amaysim2_setting_auto_topup_min_balance",limit.toString());
			browser.selectOptionForSelect("my_amaysim2_setting_auto_topup_amount",rechargeVal.toString());
		} else {
			browser.clickElement(By.xpath("//label[text()='Allow call forwarding']/following-sibling::div//input[@value='false']"));
		}
		return saveForm(AUTO_RECHARGE);
	}
	
	private AccSettingsPage clickEdit(String fieldLabel) {
		browser.clickElement("//div[div/div/text()='" + fieldLabel + "']/following-sibling::div//a[text()='Edit']");
		finishLoading();
		return this;
	}
	
	private AccSettingsPage setField(String field, String value) {
		browser.inputText(By.xpath("//label[text()='" + field + "']/following-sibling::div/input"), value);
		return this;
	}
	
	private AccSettingsPage setCheckbox(String field, boolean enable) {
		if (isChecked(field) != enable) {
			browser.clickElement(By.xpath("//div[text()='" + field + "']/following-sibling::div//input[@type='checkbox']/following-sibling::label"));
			finishLoading();
		};
		return this;
	}
	
	private boolean isChecked(String fieldLabel) {
		return browser.doesElementExist(By.xpath("//div[text()='" + fieldLabel + "']/following-sibling::div//input[@type='checkbox' and @checked]"));
	}
	
	private AccSettingsPage save(String field) {
		browser.clickElement(By.xpath("//div[label[text()='" + field +"']]//following-sibling::div//input[@name='commit']"));
		finishLoading();
		return this;
	}
	
	private AccSettingsPage saveForm(String field) {
		browser.clickElement(By.xpath("//div[div[div[div[text()='" + field +"']]]]/following-sibling::form//input[@name='commit']"));
		finishLoading();
		return this;
	}
	
	private String getValue(String fieldLabel) {
		return browser.getText("//div[text()='" + fieldLabel + "']/following-sibling::div");
	}
	
	private String getBoldValues(String fieldLabel) {
		return browser.getText("//div[div[div[text()='" + fieldLabel + "']]]/following-sibling::div[1]").replace("/n", "");
	}
}
