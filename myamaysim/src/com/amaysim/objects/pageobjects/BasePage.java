package com.amaysim.objects.pageobjects;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.amaysim.config.AutomationProperties;


/**
 * Describes the base page object
 * 
 * @author ecbello
 */
public class BasePage {
	private WebDriver driver;
	public String baseUrl;
	
	public BasePage() {
		String browser;

		try {
			Properties prop = AutomationProperties.getProperties();
			browser = prop.getProperty("browser");
		} catch (Exception e) {
			e.printStackTrace();
			browser = "chrome";
		}
		
		switch(browser.toLowerCase()) {
			case "chrome" :
			case "google chrome" :
				System.setProperty("webdriver.chrome.driver", "src/com/amaysim/config/drivers/chromedriver.exe");
				driver = new ChromeDriver();
				break;
			case "firefox" :
			case "ff" :
			case "mozilla firefox" :
				driver = new FirefoxDriver();
				break;
			case "edge" :
			case "ie" :
			case "internet explorer" :
				driver = new EdgeDriver();
		}
		driver.manage().window().maximize();
	}
	
	public BasePage navigate(String url) {
		String finalUrl = url.startsWith("/") ? baseUrl + url : url;
		System.out.println(finalUrl);
		driver.get(finalUrl);
		return this;
	}
	
	public void close() {
		driver.close();
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
	public String getAttributeValue(By locator, String attr) {
		waitUntilElementIsVisible(locator, 2);
		return findElement(locator).getAttribute(attr);
	}
	
	public String getText(By locator) {
		waitUntilElementIsVisible(locator, 2);
		return findElement(locator).getText();
	}
	
	public String getText(String xpath) {
		return getText(By.xpath(xpath));
	}
	
	public BasePage inputText(By locator, String text) {
		waitUntilElementIsVisible(locator, 2);
		WebElement elem = findElement(locator);
		elem.clear();
		elem.sendKeys(text);
		return this;
	}
	
	public BasePage inputText(String xpath, String text) {
		return inputText(By.xpath(xpath), text);
	}
	
	public BasePage clickElement(By locator) {
		waitUntilElementIsVisible(locator, 2);
		WebElement elem = findElement(locator);
		try {
			elem.click();
		} catch (WebDriverException e) {
			((JavascriptExecutor) driver).executeScript("javascript:window.scrollBy(0,-250)");
			elem.click();
		}
		return this;
	}
	
	public BasePage clickElement(String xpath) {
		return clickElement(By.xpath(xpath));
	}
	
	public BasePage refresh() {
		driver.navigate().refresh();
		return this;
	}
	
	public String getCurrentUrl() {
		String currUrl = driver.getCurrentUrl();
		return currUrl;
	}
	
	public Integer countElements(By locator) {
		List<WebElement> elemList = findElements(locator);
		if (elemList == null) {
			return 0;
		} else {
			return elemList.size();
		}
	}
	
	public Integer countElements(String xpath) {
		return countElements(By.xpath(xpath));
	}
	
	public List<WebElement> findElements(By locator) {
		try {	
			List<WebElement> elementList = driver.findElements(locator);
			return elementList;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<WebElement> findElements(String xpath) {
		return findElements(By.xpath(xpath));
	}

	public WebElement findElement(By locator) {
		try {	
			WebElement element = driver.findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			return element;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public WebElement findElement(String xpath) {
		return findElement(By.xpath(xpath));
	}
	
	public boolean doesElementExist(By locator) {
		try {
			return driver.findElement(locator) != null;
		} catch (NoSuchElementException noElem) {
			return false;
		}
	}
	
	public boolean doAllElementsExist(By[] locatorList) {
		for (By locator : locatorList) {
			if (!doesElementExist(locator)) {
				return false;
			}
		}
		return true;
	}
	
	public BasePage setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
		return this;
	}
	
	public BasePage waitUntilElementIsVisible(By locator, int seconds) {
		int i=0;
		while(i++<seconds+1) {
			try {
				driver.findElement(locator);
				return this;
			} catch (NoSuchElementException e) {
				try {
					Thread.sleep(1000);
					continue;
				} catch (Exception e1){
					e1.printStackTrace();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return this;
	}
	
	public BasePage waitUntilElementIsNotVisible(By locator, int seconds) {
		int i=0;
		while(i++<seconds+1) {
			try {
				driver.findElement(locator);
				Thread.sleep(1000);
				continue;
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				return this;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return this;
	}
    
	public BasePage clickLink(String linkText) {
		clickElement(By.linkText(linkText));
		return this;
	}
	
	public BasePage selectOptionForSelect(String selectId, String optVal) {
		clickElement(By.id(selectId)).clickElement(By.xpath("//select[@id='" + selectId + "']/option[@value='" + optVal + "']"));
		return this;
	}
	
	public String executeJavascript(String script) {
		return (String)((JavascriptExecutor) driver).executeScript(script);
	}
	
	public BasePage setWindowSize(Integer width, Integer height) {
		driver.manage().window().setSize(new Dimension(width, height));
		return this;
	}
	
	public Dimension getWindowSize() {
		return driver.manage().window().getSize();
	}
}