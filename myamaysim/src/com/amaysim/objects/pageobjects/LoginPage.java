package com.amaysim.objects.pageobjects;

import org.openqa.selenium.By;

import com.amaysim.objects.dataobjects.UserData;
import com.amaysim.objects.pageobjects.myaccount.AccOverviewPage;

public class LoginPage extends AmaysimApp {
	public LoginPage(BasePage browser) {
		super(browser);
	}
	
	public AccOverviewPage login(UserData userdata) {
		return login(userdata.getSimNumber(), userdata.getPassword());
	}
	
	public AccOverviewPage login(String phoneNumber, String password) {
		browser.inputText(By.id("mobile_number"), phoneNumber);
		browser.inputText(By.id("password"), password);
		browser.clickElement(By.xpath("//input[@value='Login']"));
		browser.waitUntilElementIsVisible(By.xpath("//body[@class='logged-in-customer']"), 10);
		return new AccOverviewPage(browser);
	}
}
