package com.amaysim.objects.pageobjects;

import com.amaysim.objects.pageobjects.myaccount.MyAccountPage;

public class HomePage extends AmaysimApp {
	private boolean isLoggedIn;
	
	public HomePage(BasePage browser) {
		super(browser);
		navigate();
	}
	
	public HomePage(BasePage browser, boolean isLoggedIn) {
		super(browser);
		this.isLoggedIn = isLoggedIn;
	}
	
	public AmaysimApp goToMyAccount() {
		browser.clickElement("//span[text()='Login']");
		if (isLoggedIn) {
			return new MyAccountPage(browser);
		} else {
			return new LoginPage(browser);
		}
	}
}
