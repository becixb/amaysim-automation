package com.amaysim.objects.dataobjects;

import java.io.FileReader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserData {
    private String simNumber = null;
    private String password = null;
    
    public UserData(String user) throws Exception {
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = parser.parse(new FileReader("src/com/amaysim/data/credentials.json")).getAsJsonObject().getAsJsonObject(user);
        this.simNumber = jsonObj.get("number").getAsString();
        this.password = jsonObj.get("password").getAsString();	
    }
    
    public String getSimNumber() {
    	return simNumber;
    }
    
    public String getPassword() {
    	return password;
    }
}
