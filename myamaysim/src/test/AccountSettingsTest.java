package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.amaysim.objects.dataobjects.UserData;
import com.amaysim.objects.pageobjects.BasePage;
import com.amaysim.objects.pageobjects.HomePage;
import com.amaysim.objects.pageobjects.LoginPage;
import com.amaysim.objects.pageobjects.enums.PaymentType;
import com.amaysim.objects.pageobjects.myaccount.AccOverviewPage;
import com.amaysim.objects.pageobjects.myaccount.AccSettingsPage;
import com.amaysim.test.AmaysimTest;

public class AccountSettingsTest extends AmaysimTest {
	BasePage browser;
	AccOverviewPage overview;
	AccSettingsPage settings;
	static String defNickname;
	static PaymentType defPayment;
	static Integer defPin;
	static boolean defCallerId;
	static boolean defCallWaiting;
	static boolean defVoiceMail;
	static boolean defUsageAlerts;
	static boolean defIntRoaming;
	static boolean defCallFwd;
	static String defCallFwdNumber;
	static Integer defSmsLimit;
	static boolean defAutoRecharge;
	static Integer defRechargeVal;
	static Integer defRechargeLim;
	
	@BeforeSuite
	public void beforeSuite() throws Exception {
		browser = new BasePage();
		overview = ((LoginPage) new HomePage(browser).goToMyAccount()).login(new UserData("test"));
		overview.closeWelcomePopup();
		settings = overview.openAccountSettings();
		defNickname = settings.getSimNickname();
		defPayment = settings.getPaymentType();
		defPin = Integer.parseInt(settings.getRechargePin());
		defCallerId = settings.isCallerIdEnabled();
		defCallWaiting = settings.isCallWaitingEnabled();
		defVoiceMail = settings.isVoicemailEnabled();
		defUsageAlerts = settings.isUsageAlertsEnabled();
		defIntRoaming = settings.isIntRoamingEnabled();
		defCallFwd = settings.isCallForwardingEnabled();
		defCallFwdNumber = settings.getCallForwardingNumber();
		defSmsLimit = Integer.parseInt(settings.getPremiumSmsLimit().replace("$", ""));
		defAutoRecharge = settings.isAutoRechargeEnabled();
		defRechargeVal = Integer.parseInt(settings.getAutoRechargeSettings().get("recharge").replace("$", ""));
		defRechargeLim = Integer.parseInt(settings.getAutoRechargeSettings().get("limit").replace("$", ""));
	}
	
	@Test
	public void verifyChangedSettings() {
		String nickname = "Eloy Bello";
		PaymentType payment = PaymentType.CREDIT_DEBIT_CARD;
		Integer pin = 1234;
		boolean callerId = true;
		boolean callWaiting = true;
		boolean voiceMail = true;
		boolean usageAlerts = true;
		boolean intRoaming = true;
		boolean callFwd = true;
		String callFwdNumber = "029876543219876";
		Integer smsLimit = 80;
		boolean autoRecharge = true;
		Integer rechargeVal = 20;
		Integer rechargeLim = 4;
		
		settings = overview.openAccountSettings();
		settings.setSimNickname(nickname)
				.setPlanSettings(payment)
				.setRechargePin(pin)
				.setCallerId(callerId)
				.setCallWaiting(callWaiting)
				.setVoicmail(voiceMail)
				.setUsageAlerts(usageAlerts)
				.setInternationalRoaming(intRoaming)
				.setCallForwarding(callFwd, callFwdNumber)
				.setPremiumSmsLimit(smsLimit)
				.setAutoRecharge(autoRecharge, rechargeLim, rechargeVal);
		
		verifyEqual("SIM Nickname verification.",settings.getSimNickname(),nickname);
		verifyEqual("Plan Settings verification.",settings.getPaymentType(),PaymentType.BPAY_VOUCHERS); // setPlanSettings() doesn't change payment type
		verifyEqual("Recharge PIN verification.",settings.getRechargePin(),pin.toString());
		verifyEqual("PUK Code verification.",settings.getPukCode(),"59956654");
		verifyEqual("Caller ID verification.",settings.isCallerIdEnabled(),callerId);
		verifyEqual("Call Waiting verification.",settings.isCallWaitingEnabled(),callWaiting);
		verifyEqual("Voicemail verification.",settings.isVoicemailEnabled(),voiceMail);
		verifyEqual("Usage Alerts verification.",settings.isUsageAlertsEnabled(),usageAlerts);
		verifyEqual("International Roaming verification.",settings.isIntRoamingEnabled(),intRoaming);
		verifyEqual("Call Forwarding verification.",settings.isCallForwardingEnabled(),callFwd);
		verifyEqual("Call Forwarding number verification.",settings.getCallForwardingNumber(),callFwdNumber);
		verifyEqual("Premium SMS Limit verification.",settings.getPremiumSmsLimit(),smsLimit.toString());
		verifyEqual("Auto-recharge verification.",settings.isAutoRechargeEnabled(),autoRecharge);
		verifyEqual("Auto-recharge limit verification.",settings.getAutoRechargeSettings().get("limit"),rechargeLim.toString());
		verifyEqual("Auto-recharge value verification.",settings.getAutoRechargeSettings().get("recharge"),rechargeVal.toString());
	}
	
	@Test
	public void checkInputValidity() {
		settings = overview.openAccountSettings();
		settings.setSimNickname("soAre!");
		verifyTrue("NICKNAME: Exclamation point validitiy.", doesErrorMessageExist("Please enter a valid SIM name."));
		settings = overview.openAccountSettings();
		settings.setSimNickname("youGoing@");
		verifyTrue("NICKNAME: Commercial at validity.", doesErrorMessageExist("Please enter a valid SIM name."));
		settings = overview.openAccountSettings();
		settings.setSimNickname("toHire~");
		verifyTrue("NICKNAME: Tilde validity.", doesErrorMessageExist("Please enter a valid SIM name."));
		settings = overview.openAccountSettings();
		settings.setSimNickname("me?");
		verifyTrue("NICKNAME: Question mark validity.", doesErrorMessageExist("Please enter a valid SIM name."));
		settings = overview.openAccountSettings();
		settings.setRechargePin("HIRE");
		verifyTrue("PIN: Letters validity.", doesErrorMessageExist("Please enter a 4-digit top-up PIN number."));
		settings = overview.openAccountSettings();
		settings.setRechargePin(1);
		verifyFalse("PIN: One digit validity.", doesErrorMessageExist("Please enter a 4-digit top-up PIN number."));
		settings.setRechargePin(12);
		verifyFalse("PIN: Two digits validity.", doesErrorMessageExist("Please enter a 4-digit top-up PIN number."));
		settings.setCallForwarding(true, "youwontregretit");
		settings.finishLoading();
		verifyTrue("CALL FWD: Letters validity.", doesErrorMessageExist("Please enter your phone number in the following format: 0412 345 678 or 02 1234 5678"));
		settings = overview.openAccountSettings();
		settings.setCallForwarding(true, "000011112222333");
		settings.finishLoading();
		verifyTrue("CALL FWD: Other number format validity.", doesErrorMessageExist("Please enter your phone number in the following format: 0412 345 678 or 02 1234 5678"));
		settings = overview.openAccountSettings();
	}
	
	@Test
	public void smallWindow() {
		Dimension windowSize = browser.getWindowSize();
		Integer width = windowSize.getWidth();
		Integer height = windowSize.getHeight();
		browser.setWindowSize(width/2, height);
		settings = overview.openAccountSettings();
		
		settings.setSimNickname("Small Window")
				.setRechargePin(5678)
				.setCallForwarding(true, "021122334455667");
		
		verifyEqual("SIM Nickname verification.",settings.getSimNickname(),"Small Window");
		verifyEqual("Recharge PIN verification.",settings.getRechargePin(),"5678");
		verifyTrue("Call Forwarding verification.",settings.isCallForwardingEnabled());
		verifyEqual("Call Forwarding number verification.",settings.getCallForwardingNumber(),"021122334455667");
	}
	
	@AfterSuite
	public void afterSuite() {
		settings = overview.openAccountSettings();
		settings.setSimNickname(defNickname)
		.setPlanSettings(defPayment)
		.setRechargePin(defPin)
		.setCallerId(defCallerId)
		.setCallWaiting(defCallWaiting)
		.setVoicmail(defVoiceMail)
		.setUsageAlerts(defUsageAlerts)
		.setInternationalRoaming(defIntRoaming)
		.setCallForwarding(defCallFwd, defCallFwdNumber)
		.setPremiumSmsLimit(defSmsLimit)
		.setAutoRecharge(defAutoRecharge, defRechargeLim, defRechargeVal);
	}
	
	private boolean doesErrorMessageExist(String errorMsg) {
		return browser.doesElementExist(By.xpath("//small[contains(@class='error') and contains(text(),'" + errorMsg + "')]"));
	}
}
